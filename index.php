<?php
	session_start();
	date_default_timezone_set('America/Bogota');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prueba desarrollo</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v6.0.0-beta2/css/all.css">

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h2>Listado de registros</h2>
				<button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#agregar">Agregar <i class="fas fa-plus"></i></button>
			</div>
		</div><br>

		<div class="col-lg-12">
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Fecha</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i=0;
						foreach($_SESSION['registros'] as $value){
					?>
						<tr>
							<td><?php echo $value['nombre']; ?></td>
							<td><?php echo $value['apellido']; ?></td>
							<td><?php echo $value['fecha']; ?></td>
							<td>
								<button class="btn btn-info editar" title="Editar" pos="<?php echo $i; ?>"><i class="fas fa-edit"></i></button>
								<button class="btn btn-danger eliminar" title="Eliminar" pos="<?php echo $i; ?>"><i class="fas fa-trash"></i></button>
							</td>
						</tr>
					<?php
							$i++;
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- modal agregar -->
	<div class="modal fade" tabindex="-1" id="agregar">
		<div class="modal-dialog">
		    <div class="modal-content">
		    	<form id="formAgregar" method="post" action="service.php" >
			      	<div class="modal-header">
			        	<h5 class="modal-title">Agregar registro</h5>
			        	<button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
			      	</div>
			      	<div class="modal-body">
			        	<div class="form-group">
			        		<label>Nombre</label>
			        		<input type="text" name="nombre" class="form-control" required>
			        	</div>
			        	<div class="form-group">
			        		<label>Apellido</label>
			        		<input type="text" name="apellido" class="form-control" required>
			        	</div>
			        	<div class="form-group">
			        		<label>Fecha registro</label>
			        		<input type="text" name="fecha" class="form-control" required readonly value="<?php echo date("Y-m-d H:i:s"); ?>">
			        	</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="submit" class="btn btn-primary">Guardar</button>
			        	<input type="hidden" name="task" value="insertarRegistro">
			      	</div>
			      </form>
		    </div>
		</div>
	</div>
	<!-- fin modal agregar -->

	<!-- modal editar -->
	<div class="modal fade" tabindex="-1" id="editar">
		<div class="modal-dialog">
		    <div class="modal-content">
		    	<form method="post" action="service.php" >
			      	<div class="modal-header">
			        	<h5 class="modal-title">Editar registro</h5>
			        	<button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
			      	</div>
			      	<div class="modal-body">
			        	<div class="form-group">
			        		<label>Nombre</label>
			        		<input type="text" name="nombre" id="nombre" class="form-control" required>
			        	</div>
			        	<div class="form-group">
			        		<label>Apellido</label>
			        		<input type="text" name="apellido" id="apellido" class="form-control" required>
			        	</div>
			        	<div class="form-group">
			        		<label>Fecha registro</label>
			        		<input type="text" id="fecha" class="form-control" required readonly>
			        	</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="submit" class="btn btn-primary">Actualizar</button>
			        	<input type="hidden" name="task" value="editarRegistro">
			        	<input type="hidden" name="pos" id="pos">
			      	</div>
			      </form>
		    </div>
		</div>
	</div>
	<!-- fin modal editar -->

	<script type="text/javascript">
		$(".eliminar").click(function(){
			pos = $(this).attr('pos');
			$.ajax({
				url: 'service.php',
				type: 'post',
				data: {
					task: 'eliminarRegistro',
					pos
				}
			}).done(function(){
				location.reload();
			})
		});

		$(".editar").click(function(){
			pos = $(this).attr('pos');
			$.ajax({
				url: 'service.php',
				type: 'post',
				data: {
					task: 'getRegistro',
					pos
				}
			}).done(function(data){
				data = JSON.parse(data);
				$("#nombre").val(data.nombre);
				$("#apellido").val(data.apellido);
				$("#fecha").val(data.fecha);
				$("#pos").val(pos);

				$("#editar").modal('show');
			})
		})
	</script>

</body>
</html>