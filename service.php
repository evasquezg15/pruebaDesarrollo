<?php
	session_start();

	$task = $_POST['task'];

	switch($task){
		case 'editarRegistro':
			$pos = $_POST['pos'];
			unset($_POST['pos']);
			foreach($_POST as $key => $value){
				$_SESSION['registros'][$pos][$key] = $value;
			}
			echo '<script>window.location.href="index.php";</script>';
		break;

		case 'getRegistro':
			echo json_encode($_SESSION['registros'][$_POST['pos']]);
		break;

		case 'eliminarRegistro':
			unset($_SESSION['registros'][$_POST['pos']]);
			$_SESSION['registros'] = array_values($_SESSION['registros']);
		break;

		case 'insertarRegistro':
			$elementos = array();
			if(!empty($_SESSION['registros'])){
				$elementos = $_SESSION['registros'];
			}

			array_push($elementos, array('nombre'=>$_POST['nombre'],'apellido'=>$_POST['apellido'],'fecha'=>$_POST['fecha']));
			$_SESSION['registros'] = $elementos;

			echo '<script>window.location.href="index.php";</script>';
		break;
	}
?>